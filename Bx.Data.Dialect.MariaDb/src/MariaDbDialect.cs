using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Bx.Data.Dialect.MariaDb;

public class MariaDbDialect : AbstractDialect
{
    private readonly string _connectionStr;

    public MariaDbDialect(string host, int port, string database, string username, string password)
    {
        _connectionStr = $"Server={host};Port={port};Database={database};Uid={username};password={password}";
    }

    public override bool InsertReturn => true;
    public override string LastIdQuery => "SELECT LAST_INSERT_ID();";

    public override string InsertQuery => "INSERT INTO {table}({columns}) VALUES({values}) RETURNING {return};";

    public override DbConnection CreateConnection()
    {
        return new MySqlConnection(_connectionStr);
    }

    public override object CreateParameter(string name, object? value)
    {
        return new MySqlParameter(name, value);
    }
}
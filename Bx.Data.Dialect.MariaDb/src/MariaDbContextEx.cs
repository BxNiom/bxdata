namespace Bx.Data.Dialect.MariaDb;

public static class MariaDbContextEx
{
    public static DbContext UseMariaDb(this DbContext context, string host, int port, string database, string username,
        string password)
    {
        context.Dialect = new MariaDbDialect(host, port, database, username, password);
        return context;
    }
}